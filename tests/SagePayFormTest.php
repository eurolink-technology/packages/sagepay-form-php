<?php

namespace Eurolink\SagePayForm;

class SetupTest extends \PHPUnit\Framework\TestCase
{
    public function testConstructor()
    {
        $vendor = 'testing-vendor';

        $sagePay = new \Eurolink\SagePayForm\Builder([
            'isProduction' => FALSE,
            'encryptPassword' => '55a51621a6648525',
            'vendor' => $vendor,
        ]);

        $sagePay->setVendorTxCode( 'TxCode-1310917599-223087284' );
        $sagePay->setCurrency( 'GBP' );
        $sagePay->setAmount( '36.95' );
        $sagePay->setDescription( 'description' );
        $sagePay->setCustomerName( 'Fname Surname' );
        $sagePay->setCustomerEMail( 'customer@example.com' );
        $sagePay->setBillingSurname( 'Surname' );
        $sagePay->setBillingFirstnames( 'Fname' );
        $sagePay->setBillingAddress1( 'BillAddress Line 1' );
        $sagePay->setBillingCity( 'BillCity' );
        $sagePay->setBillingPostCode( 'W1A 1BL' );
        $sagePay->setBillingCountry( 'GB' );
        $sagePay->setBillingPhone( '447933000000' );
        $sagePay->setSuccessURL( 'https://example.com/success' );
        $sagePay->setFailureURL( 'https://example.com/failure' );
        $sagePay->setDeliverySameAsBilling();

        $crypt = $sagePay->getCrypt();

        $this->assertStringStartsWith('https://', $sagePay->getFormEndpoint());

        $this->assertEquals('3.00', $sagePay->getVPSProtocol());

        $this->assertEquals('PAYMENT', $sagePay->getTxType());

        $this->assertStringStartsWith('@', $crypt);

        $this->assertEquals($vendor, $sagePay->getVendorCode());

        $target = '@2DCD27338114D4C39A14A855702FBAB2EF40BCAC2D76A3ABC0F660A07E9C1C924D625E626F1284BBED349A0809A343CE30546619599D25A9F819D9A00EFB6C4306744B7C9B5BA3A5B495DFF5E2B79F877D7DF209A04AC2D6E8C0DBCA8A51C18A7DC2455BEE8D92EE82C90F61AB5506059338D09F275CB7E04854E83AC54454F5F1B3AEFBC42D476B895334090B6CB1071C329CA34561A12A67C27ED1623B70BB479F79364E484A383EDD3C5B69F8B73A8E5D5F842E2F18BAE21E173AD85934E43878AF80C13EC07417C9CE0BD00B5D9D9EC351A6B38D157E0BFDEF4A5611A9DCB535CF69005254C6D5F93B998061659DA3E7A6B1EE7BC4CD493BD182EDC9143356B3060FCCAD7DF59FDED42FB512BA5983F65163D528A88EBDBF15594094837FB11808F15D25D7DAD2CB334D10FBB0AAC369B34A224EF822938982DBC50304267C527DC9710F6F87EA3EF2ABEF0BA0C42F004A4A3BFA49335B9B3EB1028768B212B8FB950E633E16B0CD3534174593C523CBCA1BA91A44E45032CD9039792EF43245C4223222FA2FCFC766CEF22DCAC2E3AD88183AA9A2384A9467F020643688177E1B948E842CC26481ED3453DFC7CD14B873FC3CD8FB30D3735049849A17CB80059FB1965738AE420041ED021281E28E57FB93E4CFD0884B338BDE296C6CAF7268122A72FC32F7D1349DED083EBEA9230BBDFF121DDD084755080CBDF1AD46EB9908D3E2C96A2954410D9937FAC87DA6A6F207ABFFEC95BDAE5E4F8A37AF3545565215A4DF1942E7CFA6BC99FEB53B43CF6D9BEB8446B6C6AF6E5800C985C10EE6EA06D8EEE9A88D75456FCDC525BD78A555F22988E0E9E15444AAD589C1B52B8565FC9FDEEE89BCA60C457ACC7B5D15F5EEBD518C78247F9B4DBD24BAE5288C0C327C044C566442841D6EB51090D72336AD543FE21014C53349FD23D5E621D0471657131DF69078EB65C832396DCF1C467511663792463B7AB6BFDEE34E0E8163364ACE9DF52FFA3CD2041B19F121';

        $this->assertEquals($target, $crypt);

        $decrypted = $sagePay->decode($crypt);

        $this->assertArrayHasKey('VendorTxCode', $decrypted);
        $this->assertArrayHasKey('Amount', $decrypted);
        $this->assertArrayHasKey('Currency', $decrypted);
        $this->assertArrayHasKey('SuccessURL', $decrypted);
        $this->assertArrayHasKey('FailureURL', $decrypted);
    }
}