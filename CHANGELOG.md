# Changelog

All notable changes to `sagepay-form-php` will be documented in this file

## 1.0.0 - 2018-10-20

- initial release

## 1.0.1 - 2018-10-22

- update README

## 1.0.2 - 2020-04-27

- add extra fields
- update PHP packages
- update encrypt and decrypt methods
- improve phpunit tests